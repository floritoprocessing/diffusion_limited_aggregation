/*
DLA - Diffusion Limited Aggregation
*/

ArrayList cloud = new ArrayList();
Emitter emitter;
PImage canvas;

boolean showDust = true;

String date = "DLA04_"
  + nf(year(),4)+nf(month()+1,2)+nf(day(),2)+"_"
  + nf(hour(),2)+nf(minute(),2)+nf(second(),2);
boolean settingsSaved=false;

PFont font;

void setup() {
  size(1024,576);
  frameRate(1000);
  
  font = loadFont("Verdana-Bold-12.vlw");
  textFont(font,12);
  
  canvas = createImage(width,height,RGB);
  for (int x=-1;x<=1;x++) for (int y=-1;y<=1;y++) {
    canvas.set((int)(width/2)+x,(int)(height/2)+y,color(255));
  }
  
  emitter = new Emitter(width/2,height/2,DustSettings.EMITTER_RADIUS,DustSettings.EMITTER_SPEED * PI/180.0);
  
}

void keyPressed() {
  if (key=='s') {
    showDust = !showDust;
  }
  else if (key=='S') {
    saveFrameWithSettings();
  }
}

void saveFrameWithSettings() {
  String frameNr = nf(frameCount,6);
  String imageName = date+"_frame"+frameNr+".bmp";
  println("Saving to \""+imageName+"\"");
  canvas.save(imageName);
  if (!settingsSaved) {
    println("Saving settings to \""+date+"_settings.txt\"");
    saveStrings(date+"_settings.txt",new DustSettings().toStrings());
    settingsSaved = true;
  }
}

void draw() {
  
  /*
  *  emitter
  */
  emitter.create();
  emitter.move();
 
  
  for (int i=0;i<cloud.size();i++) {
    ((Dust)cloud.get(i)).traceAndKill(canvas);
  }
  
  for (int i=0;i<cloud.size();i++) {
    if (((Dust)cloud.get(i)).dead) {
      cloud.remove(i--);
    }
  }
  
  image(canvas,0,0);
  
  if (showDust) {
    for (int i=0;i<cloud.size();i++) {
      ((Dust)cloud.get(i)).draw();
    }
  }
  
  for (int i=0;i<cloud.size();i++) {
    ((Dust)cloud.get(i)).move();
  }
  
  fill(255,0,0);
  text("fps: "+nf(frameRate,2,1)+", frame "+frameCount
    +", dustAmount: "+cloud.size(),15,height-15);
  
  if (frameCount==1||frameCount%1000==0) {
    saveFrameWithSettings();
  }
}
