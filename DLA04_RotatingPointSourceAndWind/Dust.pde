class Dust {
  
  float x, y;
  float rd, sp;
  boolean dead=false;
  
  Dust(float x, float y) {
    this.x=x;
    this.y=y;
    rd = random(TWO_PI);
    sp = DustSettings.SPEED;
  }
  
  void move() {
    rd += random(-DustSettings.TURN,DustSettings.TURN);
    x += sp*cos(rd) + DustSettings.WIND_X;
    y += sp*sin(rd) + DustSettings.WIND_Y;
    if (x<-200||x>=width+200||y<-200||y>+height+200) {
      dead=true;
    }
  }
  
  void traceAndKill(PImage img) {
    int ix = (int)x;
    int iy = (int)y;
    for (int sy=iy-1; sy<=iy+1; sy++) {
      for (int sx=ix-1; sx<=ix+1; sx++) {
        if ((img.get(sx,sy)&0xff)==255) {
          img.set(ix,iy,0xffffffff);
          dead=true;
          return;
        }
      }
    }
  }
  
  void draw() {
    set((int)x,(int)y,0xff808080);
  }
  
}
