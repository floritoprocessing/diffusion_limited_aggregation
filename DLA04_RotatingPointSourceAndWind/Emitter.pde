class Emitter {
  
  float x, y;
  float radius;
  float rdSpeed;
  float rd = 0;
  
  Emitter(float x, float y, float radius, float rdSpeed) {
    this.x = x;
    this.y = y;
    this.radius = radius;
    this.rdSpeed = rdSpeed;
  }
  
  void move() {
    rd += rdSpeed;
  }
  
  void create() {
    float percentageFilled = (float)cloud.size()/DustSettings.MAX_DUST; // (0..1)
    float emitterStrength = 1 - percentageFilled; // --> 1..0
    int emitterAmount = (int)(DustSettings.EMITTER*emitterStrength);
    for (int i=0;i<emitterAmount;i++) {
      cloud.add(new Dust(x+radius*cos(rd),y+radius*sin(rd)));
    }
  }
  
}
