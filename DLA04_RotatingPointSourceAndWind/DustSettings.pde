class DustSettings {

  static final int EMITTER = 50;
  static final int MAX_DUST = 50000;
  static final float EMITTER_RADIUS = 250;
  static final float EMITTER_SPEED = 1.3;
  static final float SPEED = 1;
  static final float TURN = 45 * PI/180.0;
  static final float WIND_X = 0.8;
  static final float WIND_Y = 0.45;
  
  String[] toStrings() {
    String[] out = new String[8];
    out[0] = "static final int EMITTER = "+EMITTER+";";
    out[1] = "static final int MAX_DUST = "+MAX_DUST+";";
    out[2] = "static final float EMITTER_RADIUS = "+EMITTER_RADIUS+";";
    out[3] = "static final float EMITTER_SPEED = "+EMITTER_SPEED+";";
    out[4] = "static final float SPEED = "+SPEED+";";
    float deg = TURN * 180/PI;
    out[5] = "static final float TURN = "+deg+" * PI/180.0;";
    out[6] = "static final float WIND_X = "+WIND_X+";";
    out[7] = "static final float WIND_Y = "+WIND_Y+";";
    return out;
  }
  
}
