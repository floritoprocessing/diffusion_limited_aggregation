
int w = 2000, h = 2000;
int[][] test1;
int[] test2;

void setup() {
  size(200,200);
  test1 = new int[w][h];
  int ms = millis();
  for (int c=0;c<10;c++) {
    for (int y=0;y<h;y++) {
      for (int x=0;x<w;x++) {
        test1[x][y] = 0;
      }
    }
  }
  println((millis()-ms));
  
  test2 = new int[w*h];
  ms = millis();
  for (int c=0;c<10;c++) {
    int i=0;
    for (int y=0;y<h;y++) {
      for (int x=0;x<w;x++) {
        i = y*w + x;
        test2[i] = 0;
        //i++;
      }
    }
  }
  println((millis()-ms));
}

void draw() {
}
