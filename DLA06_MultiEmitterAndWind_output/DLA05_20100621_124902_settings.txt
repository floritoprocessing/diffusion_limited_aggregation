final int MAX_DUST = 50000;
final EmitterSetting[] EMITTER_TYPES = {
  new EmitterSetting(96,  38.95131*PI/180.0, 1.3800796, color(202,170,150)),
  new EmitterSetting(96,  151.1009*PI/180.0, 0.9074052, color(204,50,191)),
  new EmitterSetting(65,  83.78169*PI/180.0, 1.5491965, color(116,115,62)),
  new EmitterSetting(59,  23.873516*PI/180.0, 3.5096676, color(244,88,79)),
  new EmitterSetting(105,  94.76097*PI/180.0, 4.0922375, color(174,113,199)),
};
final float WIND_X = -0.18425874;
final float WIND_Y = -0.30799899;
