final int MAX_DUST = 50000;
final EmitterSetting[] EMITTER_TYPES = {
  new EmitterSetting(24,  6.315315*PI/180.0, 2.213967, color(134.0,43.0,229.0)),
  new EmitterSetting(30,  21.868813*PI/180.0, 3.5424562, color(78.0,183.0,123.0)),
  new EmitterSetting(49,  8.452831*PI/180.0, 2.6059272, color(251.0,210.0,176.0)),
};
final float WIND_X = 0.30612224;
final float WIND_Y = -0.43657166;
