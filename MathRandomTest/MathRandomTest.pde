Obj[] objects = new Obj[100];

void setup() {
  size(400,400);
  frameRate(1000);
  rectMode(CENTER);
  noFill();
  for (int i=0;i<objects.length;i++) {
    objects[i] = new Obj();
  }
}

void draw() {
  background(255);
  float x=0;
  for (int i=0;i<objects.length;i++) {
    rect(objects[i].x,objects[i].y,50,50);
    objects[i].x += (Math.random()-0.5);
    x += objects[i].x;
  }
  x/=objects.length;
  println(x);
  
}
