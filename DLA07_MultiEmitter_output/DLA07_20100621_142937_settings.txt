final int MAX_DUST = 25000;
final EmitterSetting[] EMITTER_TYPES = {
  new EmitterSetting(58,  30.67412*PI/180.0, 3.8779752, color(152,218,225)),
  new EmitterSetting(125,  138.45294*PI/180.0, 0.1258052, color(166,127,181)),
  new EmitterSetting(138,  22.654646*PI/180.0, 0.70494497, color(87,131,220)),
  new EmitterSetting(59,  142.26071*PI/180.0, 1.6213348, color(182,231,130)),
  new EmitterSetting(135,  126.55318*PI/180.0, 1.7022821, color(107,67,161)),
  new EmitterSetting(81,  27.996372*PI/180.0, 0.8849657, color(252,245,194)),
};
