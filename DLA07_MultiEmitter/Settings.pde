class Settings {
  
  int MAX_DUST = 25000;
  EmitterSetting[] EMITTER_TYPES;
  
  Settings() {
    int emitterAmount = (int)random(3,7);
    EMITTER_TYPES = new EmitterSetting[emitterAmount];
    for (int i=0;i<emitterAmount;i++) {
      EMITTER_TYPES[i] = new EmitterSetting(
        (int)random(40,150),
        random(180)*PI/180.0,
        random(0.5,5),
        color(random(40,256),random(40,256),random(40,256))
      );
    }
  }
  
  String[] toStrings() {
    ArrayList out = new ArrayList();
    out.add("final int MAX_DUST = "+MAX_DUST+";");
    out.add("final EmitterSetting[] EMITTER_TYPES = {");
    for (int i=0;i<EMITTER_TYPES.length;i++) {
    out.add("  new EmitterSetting("+EMITTER_TYPES[i].amount
            +",  "+(EMITTER_TYPES[i].turn*180.0/PI)+"*PI/180.0"
            +", "+EMITTER_TYPES[i].speed
            +", color("+(int)red(EMITTER_TYPES[i].colour)+","+(int)green(EMITTER_TYPES[i].colour)+","+(int)blue(EMITTER_TYPES[i].colour)+")),");
    }
    out.add("};");
    return (String[])out.toArray(new String[0]);
  }
  
}
