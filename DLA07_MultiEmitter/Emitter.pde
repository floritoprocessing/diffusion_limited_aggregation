class Emitter {
  
  float x, y;
  EmitterSetting setting;
  
  Emitter(float x, float y, EmitterSetting setting) {
    this.x = x;
    this.y = y;
    this.setting = setting;
  }
  
  void create() {
    float percentageFilled = (float)cloud.size()/settings.MAX_DUST; // (0..1)
    float emitterStrength = 1 - percentageFilled; // --> 1..0
    int emitterAmount = (int)(setting.amount*emitterStrength/(float)settings.EMITTER_TYPES.length);
    for (int i=0;i<emitterAmount;i++) {
      cloud.add(new Dust(x, y, setting.turn, setting.speed, setting.colour));
    }
  }
  
}
