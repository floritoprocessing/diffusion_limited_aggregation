final int MAX_DUST = 25000;
final EmitterSetting[] EMITTER_TYPES = {
  new EmitterSetting(11,  87.97813*PI/180.0, 3.323982, color(190,112,161)),
  new EmitterSetting(46,  27.46773*PI/180.0, 3.6705344, color(234,151,126)),
  new EmitterSetting(50,  1.4440477*PI/180.0, 4.426077, color(143,105,236)),
  new EmitterSetting(75,  166.2854*PI/180.0, 2.427523, color(59,152,113)),
  new EmitterSetting(45,  135.81224*PI/180.0, 2.160036, color(181,209,120)),
};
