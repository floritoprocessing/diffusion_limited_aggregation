final int MAX_DUST = 25000;
final EmitterSetting[] EMITTER_TYPES = {
  new EmitterSetting(146,  9.088591*PI/180.0, 1.7688329, color(179,95,89)),
  new EmitterSetting(95,  157.3599*PI/180.0, 1.9248154, color(230,236,178)),
  new EmitterSetting(130,  148.28583*PI/180.0, 0.6684655, color(132,199,169)),
  new EmitterSetting(56,  148.61427*PI/180.0, 3.6299248, color(195,154,115)),
  new EmitterSetting(41,  115.8558*PI/180.0, 4.6765456, color(107,157,203)),
  new EmitterSetting(125,  84.24782*PI/180.0, 1.0873877, color(188,151,164)),
  new EmitterSetting(131,  53.963825*PI/180.0, 4.4389925, color(117,226,182)),
};
