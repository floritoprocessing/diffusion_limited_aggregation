class Dust {
  
  float x, y;
  float rd, sp;
  
  Dust(boolean fromEdge) {
    if (fromEdge) {
      float pos = random(2*(width+height));
      if (pos<width) {
        x = pos;
        y = 0;
      } else if (pos<width+height) {
        x = width;
        y = pos-width;
      } else if (pos<width+height+width) {
        x = pos-width-height;
        y = height;
      } else {
        x = 0;
        y = pos-height-2*width;
      }
    } else {
      x = random(width);
      y = random(height);
    }
    
    
    rd = random(TWO_PI);
    sp = DustSettings.SPEED;
  }
  
  void move() {
    rd += random(-DustSettings.TURN,DustSettings.TURN);
    x += sp*cos(rd);
    y += sp*sin(rd);
    if (x<0) x+=width;
    if (x>=width) x-=width;
    if (y<0) y+=height;
    if (y>=height) y-=height;
  }
  
  boolean traceAndKill(PImage img) {
    int ix = (int)x;
    int iy = (int)y;
    for (int sy=iy-1; sy<=iy+1; sy++) {
      for (int sx=ix-1; sx<=ix+1; sx++) {
        if ((img.get(sx,sy)&0xff)==255) {
          img.set(ix,iy,0xffffffff);
          if (DustSettings.DIE_ON_IMPACT) {
            return true;
          } else {
            return false;
          }
        }
        
      }
    }
    return false;
  }
  
  void draw() {
    set((int)x,(int)y,0xff808080);
  }
  
}
