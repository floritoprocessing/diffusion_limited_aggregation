class DustSettings {

  static final int AMOUNT = 50000;
  static final float SPEED = 0.5;
  static final float TURN = 0.02 * PI/180.0;
  static final boolean DIE_ON_IMPACT = true;
  
  String[] toStrings() {
    String[] out = new String[4];
    out[0] = "static final int AMOUNT = "+AMOUNT+";";
    out[1] = "static final float SPEED = "+SPEED+";";
    float deg = TURN * 180/PI;
    out[2] = "static final float TURN = "+deg+" * PI/180.0;";
    out[3] = "static final boolean DIE_ON_IMPACT = "+DIE_ON_IMPACT+";";
    return out;
  }
  
}
