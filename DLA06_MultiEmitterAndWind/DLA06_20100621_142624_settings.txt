final int MAX_DUST = 50000;
final EmitterSetting[] EMITTER_TYPES = {
  new EmitterSetting(149,  88.34019*PI/180.0, 0.92461675, color(183,251,65)),
  new EmitterSetting(95,  31.64615*PI/180.0, 3.2734978, color(76,202,191)),
  new EmitterSetting(49,  108.24655*PI/180.0, 0.593254, color(149,88,60)),
  new EmitterSetting(46,  150.34702*PI/180.0, 0.36515677, color(95,89,108)),
};
final float WIND_X = 0.041841574;
final float WIND_Y = -0.041786514;
