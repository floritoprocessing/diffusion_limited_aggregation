class Settings {
  
  int MAX_DUST = 50000;
  EmitterSetting[] EMITTER_TYPES;
  float WIND_X;//0.2;
  float WIND_Y;//.11;
  
  Settings() {
    int emitterAmount = (int)random(3,6);
    EMITTER_TYPES = new EmitterSetting[emitterAmount];
    float slowestSpeed = 5;
    for (int i=0;i<emitterAmount;i++) {
      EMITTER_TYPES[i] = new EmitterSetting(
        (int)random(40,150),
        random(180)*PI/180.0,
        random(0.1,5),
        color(random(40,256),random(40,256),random(40,256))
      );
      slowestSpeed = min(slowestSpeed,EMITTER_TYPES[i].speed);
    }
    float windrd = random(TWO_PI);
    float windsp = random(slowestSpeed*0.333);
    WIND_X=windsp*cos(windrd);
    WIND_Y=windsp*sin(windrd);
  }
  
  String[] toStrings() {
    ArrayList out = new ArrayList();
    out.add("final int MAX_DUST = "+MAX_DUST+";");
    out.add("final EmitterSetting[] EMITTER_TYPES = {");
    for (int i=0;i<EMITTER_TYPES.length;i++) {
    out.add("  new EmitterSetting("+EMITTER_TYPES[i].amount
            +",  "+(EMITTER_TYPES[i].turn*180.0/PI)+"*PI/180.0"
            +", "+EMITTER_TYPES[i].speed
            +", color("+(int)red(EMITTER_TYPES[i].colour)+","+(int)green(EMITTER_TYPES[i].colour)+","+(int)blue(EMITTER_TYPES[i].colour)+")),");
    }
    out.add("};");
    out.add("final float WIND_X = "+WIND_X+";");
    out.add("final float WIND_Y = "+WIND_Y+";");
    return (String[])out.toArray(new String[0]);
  }
  
}
