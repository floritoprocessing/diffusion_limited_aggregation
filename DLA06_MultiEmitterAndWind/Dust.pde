class Dust {
  
  float x, y;
  float rd;
  
  float turn, speed;
  int colour;
  int grayColour;
  
  boolean dead=false;
  
  Dust(float x, float y, float turn, float speed, int colour) {
    this.x=x;
    this.y=y;
    rd = random(TWO_PI);
    this.turn = turn;
    this.speed = speed;
    this.colour = colour;
    grayColour = color(red(colour)/2,green(colour)/2,blue(colour)/2);
  }
  
  void move() {
    rd += random(-turn,turn);
    x += speed*cos(rd) + settings.WIND_X;
    y += speed*sin(rd) + settings.WIND_Y;
    if (x<0||x>width||y<0||y>height) {
      dead=true;
    }
  }
  
  void trace(PImage img) {
    int ix = (int)x;
    int iy = (int)y;
    int ccol;
    for (int sy=iy-1; sy<=iy+1; sy++) {
      for (int sx=ix-1; sx<=ix+1; sx++) {
        ccol = img.get(sx,sy);
        if ((ccol&0xff)!=0) {
          img.set(ix,iy,colour);
          dead=true;
          return;
        }
      }
    }
  }
  
  void draw() {
    set((int)x,(int)y,grayColour);
  }
  
}
