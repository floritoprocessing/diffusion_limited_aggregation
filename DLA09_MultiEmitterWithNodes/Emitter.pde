class Emitter {
  
  float x, y;
  EmitterSetting setting;
  int typeIndex;
  
  Emitter(float x, float y, EmitterSetting setting, int typeIndex) {
    this.x = x;
    this.y = y;
    this.setting = setting;
    this.typeIndex = typeIndex;
  }
  
  void create() {
    float percentageFilled = (float)cloud.size()/settings.MAX_DUST; // (0..1)
    float emitterStrength = 1 - percentageFilled; // --> 1..0
    int emitterAmount = (int)(setting.amount*emitterStrength/(float)emitter.size());
    for (int i=0;i<emitterAmount;i++) {
      cloud.add(new Dust(x, y, setting.turn, setting.speed, setting.colour, typeIndex));
    }
  }
  
}
