class EmitterSetting {
  
  int amount;
  float turn;
  float speed;
  int colour;
  
  EmitterSetting(int amount, float turn, float speed, int colour) {
    this.amount = amount;
    this.turn = turn;
    this.speed = speed;
    this.colour = colour;
  }
}
