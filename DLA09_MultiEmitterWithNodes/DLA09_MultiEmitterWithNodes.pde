/*
*  DLA - Diffusion Limited Aggregation
*/

int MAX_TYPES = 5; // ultimately, max 32

int nthFrameToSave = 25;
int savedFrameIndex = 0;
Settings settings = new Settings();

ArrayList cloud = new ArrayList();
ArrayList emitter = new ArrayList();//new Emitter[settings.EMITTER_TYPES.length];
PImage canvas;

/**
*  The dustMap is used to see which creature is residing where
*  Uses first bit for first dust type, second bit for second dust type etc.
*  When two different types of dust collide, a node is created
*/
PImage dustMap;
int[] emptyDustMap;

boolean showDust = true;

String date = "DLA09_"
  + nf(year(),4)+nf(month()+1,2)+nf(day(),2)+"_"
  + nf(hour(),2)+nf(minute(),2)+nf(second(),2);
boolean settingsSaved=false;

PFont font;

void setup() {
  size(800,800);
  frameRate(1000);
  
  font = loadFont("Verdana-Bold-12.vlw");
  textFont(font,12);
  
  canvas = createImage(width,height,RGB); 
  dustMap = createImage(width,height,RGB);
  emptyDustMap = new int[width*height];
  
  for (int i=0;i<settings.EMITTER_TYPES.length-1;i++) {
    float rd = (float)i/(settings.EMITTER_TYPES.length-1)*TWO_PI;
    float rad = width*0.4;
    emitter.add(new Emitter(width/2+rad*cos(rd),height/2+rad*sin(rd),settings.EMITTER_TYPES[i],i));
  }
  int i = settings.EMITTER_TYPES.length-1;
  emitter.add(new Emitter(width/2,height/2,settings.EMITTER_TYPES[i],i));
  
}

void keyPressed() {
  if (key=='s') {
    showDust = !showDust;
  }
  else if (key=='S') {
    saveFrameWithSettings(false);
  }
}

void saveFrameWithSettings(boolean screenshot) {
  //String frameNr = nf(frameCount,6);
  String frameNr = nf(savedFrameIndex,6);
  savedFrameIndex++;
  String imageName = "output/"+date+"/frame"+frameNr+".jpg";
  println("Saving to \""+imageName+"\"");
  if (screenshot) {
    save(imageName);
  } else {
    canvas.save(imageName);
  }
  
  if (!settingsSaved) {
    println("Saving settings to \"/"+date+"/settings.txt\"");
    saveStrings("output/"+date+"/settings.txt",settings.toStrings());
    settingsSaved = true;
  }
}

void draw() {
  
  /*
  *  emitter: Emit new dust
  */
  for (int i=0;i<emitter.size();i++) {
    ((Emitter)emitter.get(i)).create();
  }
 
  /*
  *  Move and trace dust
  */
  for (int i=0;i<cloud.size();i++) {
    ((Dust)cloud.get(i)).move();
    ((Dust)cloud.get(i)).trace(canvas);
  }
  
  /*
  *  Check if a trace cloggs the Emitter -> kill emitter
  */
  for (int i=0;i<emitter.size();i++) {
    int x = (int)((Emitter)emitter.get(i)).x;
    int y = (int)((Emitter)emitter.get(i)).y;
    if ((canvas.get(x,y)&0xffffff) != 0) {
      println("Emitter "+i+" removed");
      emitter.remove(i--);
    }
  }
  
  /*
  *  Update the dust map
  */
  arrayCopy(emptyDustMap,dustMap.pixels);
  //dustMap.updatePixels();
  for (int i=0;i<cloud.size();i++) {
    Dust dust = (Dust)cloud.get(i);
    int typeIndex = dust.typeIndex;
    int x = (int)dust.x;
    int y = (int)dust.y;
    int index = y*width+x;
    if (x>=0&&x<width&&y>=0&&y<height) {
      dustMap.pixels[index] = dustMap.pixels[index] | (1<<typeIndex);
    }
  }
  //dustMap.updatePixels();
  
  /*
  *  Check if each pixel in the dustMap has more than one bit filled -> multiple occupation -> draw pixel on canvas
  */
  int x=0, y=0;
  int bitCount, pix;
  for (int i=0;i<dustMap.pixels.length;i++) {
    bitCount=0;
    pix = dustMap.pixels[i];
    for (int c=0;c<MAX_TYPES;c++) {
      bitCount += (pix>>c&0x01);
    }
    if (bitCount>1) {
      canvas.set(x,y,color(255));
    }
    x++;
    if (x==width) {
      x=0;
      y++;
    }
  }
   
  /*
  *  Remove dead dust
  */
  for (int i=0;i<cloud.size();i++) {
    if (((Dust)cloud.get(i)).dead) {
      cloud.remove(i--);
    }
  }
  
  /*
  *  Now show the traces and the dust
  */
  image(canvas,0,0);
  if (showDust) {
    for (int i=0;i<cloud.size();i++) {
      ((Dust)cloud.get(i)).draw();
    }
  }
  
  if (keyPressed && key=='m') {
    dustMap.updatePixels();
    image(dustMap,0,0);
  }
  
  fill(255,0,0);
  text("fps: "+nf(frameRate,2,1)+", frame "+frameCount
    +", dustAmount: "+cloud.size(),15,height-15);
  
  if (frameCount%nthFrameToSave==0) {
    saveFrameWithSettings(true);
  }
  //} else if (frameCount==100 || frameCount==499) {
  //  saveFrameWithSettings(true);
  //}
}
