class DustSettings {

  static final int AMOUNT = 100000;
  static final float SPEED = 3;
  static final float TURN = 150 * PI/180.0;
  
  String[] toStrings() {
    String[] out = new String[3];
    out[0] = "static final int AMOUNT = "+AMOUNT+";";
    out[1] = "static final float SPEED = "+SPEED+";";
    float deg = TURN * 180/PI;
    out[2] = "static final float TURN = "+deg+" * PI/180.0;";
    return out;
  }
  
}
