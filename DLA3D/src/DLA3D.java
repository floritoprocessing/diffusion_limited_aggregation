import java.util.ArrayList;

import processing.core.PApplet;
import processing.core.PVector;


public class DLA3D extends PApplet {

	private static final long serialVersionUID = -5537496018483064255L;

	public static void main(String[] args) {
		PApplet.main(new String[] {"DLA3D"});
	}
	
	Settings settings = new Settings();
	Camera camera;
	ArrayList<Particle> particles = new ArrayList<Particle>();
	Structure structure = new Structure();
	
	float particleOpacity = 0;
	int lastFrame = 0;
	
	boolean displayParticles = true;
	boolean stats = true;
	boolean fastForward = false;
	boolean generate = true;
	
	public void setup() {
		size(settings.WIDTH, settings.HEIGHT, OPENGL);
		camera = new Camera(this, settings);
		PVector lo = settings.lowerLimit;
		PVector hi = settings.highLimit;
		PVector cen = PVector.add(lo, hi);
		cen.mult(0.5f);
		structure.add(new IntParticle(cen));
		
		for (int i=0;i<10;i++) {
			int x = (int)(cen.x + 0.2*(Math.random()-0.5)*(settings.highLimit.x-settings.lowerLimit.x));
			int y = (int)(cen.y + 0.2*(Math.random()-0.5)*(settings.highLimit.y-settings.lowerLimit.y));
			int z = (int)(cen.z + 0.2*(Math.random()-0.5)*(settings.highLimit.z-settings.lowerLimit.z));
			structure.add(new IntParticle(new PVector(x,y,z)));
		}
		
		help();
	}

	private void help() {
		println("Use mouse to change camera");
		println("[H] for help");
		println("[SPACE] to clear particles");
		println("[G] to toggle generation of particles");
		println("[F] to toggle fast forward calc");
		println("[P] to toggle display particles");
		println("[S] to toggle stats");
	}
	
	public void draw() {
		
		for (int it=0;it<(fastForward?10:1);it++) {
		
			/*
			 * Generate particles
			 */
			if (generate) {
				for (int i=0;i<settings.BIRTH_RATE;i++) {
					if (particles.size()<settings.MAX_PARTICLES) {
						particles.add(Particle.createEverywhere(settings));//AtEdge or Everywhere
					} else {
						break;
					}
				}
			}
			
			/*
			 * Move particles
			 */
			for (int i=0;i<particles.size();i++) {
				Particle p = particles.get(i);
				p.move();
				if (p.outsideSpace()) {
					particles.remove(i--);
				}
			}
			
			/*
			 * Collide particles
			 */
			for (int i=0;i<particles.size();i++) {
				Particle p = particles.get(i);
				PVector[] connection = structure.isCloseTo(p, settings.CONNECT_DISTANCE);
				if (connection!=null) {
					structure.add(new IntParticle(p,connection));
					particles.remove(i--);
				}
			}
		
		}
		
		background(255);
		//lights();
		smooth();
		camera.update();
		
		/*
		noStroke();
		fill(220,220,255,64);
		pushMatrix();
		rectMode(CORNERS);
		translate(0,-settings.lowerLimit.y,0);
		rotateX(HALF_PI);
		rect(settings.lowerLimit.x, settings.lowerLimit.z, settings.highLimit.x, settings.highLimit.z);
		popMatrix();
		*/
		
		strokeWeight(2);
		float targetOpacity = displayParticles ? 100:0;
		particleOpacity = 0.94f*particleOpacity + 0.06f*targetOpacity;
		if (particleOpacity>1) {
			stroke(0,0,0,particleOpacity);
			for (Particle p:particles) {
				p.draw(this);
			}
		}
		
		
		strokeWeight(3);
		stroke(0);
		fill(128);
		for (IntParticle p:structure) {
			p.draw(this, settings);
		}
		
		if (stats) {
			System.out.println("structure: "+structure.size()+", particles: "+particles.size());
			System.out.println("frametime: "+nf((millis()-lastFrame)/1000.0f,1,2));
		}
		
		/*if (frameCount%5==0) {
			saveFrame("E:\\DLA3D_output\\DLA3D_frame######.bmp");
		}*/
		
		lastFrame = millis();
	}

	@Override
	public void keyPressed() {
		switch (key) {
		case ' ':
			particles.clear();
			break;
		case 'p':
			displayParticles = !displayParticles;
			break;
		case 's':
			stats = !stats;
			break;
		case 'h':
			help();
			break;
		case 'f':
			fastForward = !fastForward;
			break;
		case 'g':
			generate = !generate;
			System.out.println("Generating new particles: "+(generate?"on":"off"));
			break;
		default:
			break;
		}		
	}
	
	
}
