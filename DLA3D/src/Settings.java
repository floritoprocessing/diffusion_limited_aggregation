import java.lang.reflect.Field;
import java.util.Properties;

import processing.core.PVector;

public class Settings {

	Properties properties = new Properties();
	
	/**
	 * Dimensions of screen
	 */
	public int WIDTH = 1280, HEIGHT = 720;
	/**
	 * Edges of space
	 */
	public PVector lowerLimit = new PVector(-100,-100,-100), highLimit = new PVector(100,100,100);
	/**
	 * Maximum amount of particles
	 */
	public int MAX_PARTICLES = 40000;
	/**
	 * Particle birth rate per frame
	 */
	public int BIRTH_RATE = 100;
	/**
	 * Particle Speed
	 */
	public float SPEED = 0.5f;
	/**
	 * Particle random movement: 0 (straight) .. 1 (completely random)
	 */
	public float RANDOM = 0.2f;
	/**
	 * Distance between Particles and Structure for particles to connect 
	 */
	public int CONNECT_DISTANCE = 3;

	public boolean STRUCTURE_DRAW_LINE = true;
	public boolean STRUCTURE_DRAW_BOX = false;
	
	
	public Settings() {
		/*
		 * Set properties based on fields
		 */
		Field[] fields = getClass().getFields();
		for (Field f:fields) {
			String key = f.getName();
			String value = "";
			try {
				value = f.get(this).toString();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
			properties.setProperty(key, value);
		}
		System.out.println("Settings "+properties);
	}

}
