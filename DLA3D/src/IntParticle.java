import processing.core.PApplet;
import processing.core.PVector;


public class IntParticle {
	
	int x, y, z;
	PVector src;
	PVector dst;
	
	public IntParticle(PVector pos) {
		x = (int)pos.x;
		y = (int)pos.y;
		z = (int)pos.z;
	}
	
	public IntParticle(PVector pos, PVector[] connection) {
		this(pos);
		src = new PVector((int)connection[0].x, (int)connection[0].y, (int)connection[0].z);
		dst = new PVector((int)connection[1].x, (int)connection[1].y, (int)connection[1].z);
	}

	public PVector[] isCloseTo(Particle testParticle, int minimumDistance) {
		int px = (int)testParticle.x;
		int py = (int)testParticle.y;
		int pz = (int)testParticle.z;
		
		int dx = x-px;
		int dy = y-py;
		int dz = z-pz;
		
		int d = minimumDistance;
		
		if (		dx>=-d&&dx<=d
				&& 	dy>=-d&&dy<=d
				&& 	dz>=-d&&dz<=d) {
			return new PVector[] { new PVector(x,y,z), new PVector(testParticle.x, testParticle.y, testParticle.z) };
		}
		
		
		return null;
	}

	public void draw(PApplet pa, Settings settings) {
		
		if (src!=null && dst!=null) {
			if (settings.STRUCTURE_DRAW_LINE) {
				pa.line(src.x, src.y, src.z, dst.x, dst.y, dst.z);
			}
		} else {
			pa.point(x, y, z);
		}
		
		if (settings.STRUCTURE_DRAW_BOX) {
			pa.pushMatrix();
			pa.translate(x, y, z);
			pa.box(settings.CONNECT_DISTANCE);
			pa.popMatrix();
		}

	}

}
