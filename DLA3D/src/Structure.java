import java.util.ArrayList;

import processing.core.PVector;


public class Structure extends ArrayList<IntParticle> {

	private static final long serialVersionUID = -814048237335824589L;

	private float x0=1000000, y0=1000000, z0=1000000;
	private float x1=-1000000, y1=-1000000, z1=-1000000;
	
	public Structure() {
		super();
	}
	
	

	@Override
	public boolean add(IntParticle p) {
		
		x0 = Math.min(x0,p.x);
		y0 = Math.min(x0,p.y);
		z0 = Math.min(x0,p.z);
		x1 = Math.max(x0,p.x);
		y1 = Math.max(x0,p.y);
		z1 = Math.max(x0,p.z);
		System.out.print(x0+", "+y0+", "+z0+" ... ");
		System.out.println(x1+", "+y1+", "+z1);
		
		return super.add(p);
	}



	public PVector[] isCloseTo(Particle testParticle, int minimumDistance) {
		if (testParticle.x<x0-minimumDistance || testParticle.x>x1+minimumDistance
				|| testParticle.y<y0-minimumDistance || testParticle.y>y1+minimumDistance
				|| testParticle.z<z0-minimumDistance || testParticle.z>z1+minimumDistance) {
			return null;
		}
		for (IntParticle structureParticle:this) {
			PVector[] connection = structureParticle.isCloseTo(testParticle, minimumDistance);
			if (connection!=null) {
				return connection;
			}
		}
		return null;
	}
	
}
