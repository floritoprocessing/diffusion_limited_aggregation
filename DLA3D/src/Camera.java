import java.awt.event.MouseEvent;

import processing.core.PApplet;
import processing.core.PConstants;


public class Camera implements PConstants {

	private PApplet pa;
	private Settings settings;
	
	private int mdx, mdy;
	private float camY = -0, camOffY = 0;
	private float camRotY = PI/4.0f, camRotOffY = 0;
	private float distance, distanceOff = 0;
	
	public Camera(PApplet pa, Settings settings) {
		this.pa = pa;
		this.settings = settings;
		distance = Math.max(settings.highLimit.x, settings.highLimit.z)*1.2f;
		pa.registerMouseEvent(this);
	}
	
	public void update() {
		float x = (float)(Math.cos(camRotY + camRotOffY) * (distance+distanceOff));
		float z = (float)(Math.sin(camRotY + camRotOffY) * (distance+distanceOff));
		pa.camera(x, camY+camOffY, z, 0, 0, 0, 0, 1, 0);
		camRotY += 0.01f; // 0.002
	}
	
	public void mouseEvent(MouseEvent e) {
		int id = e.getID();
		switch (id) {
		case MouseEvent.MOUSE_PRESSED:
			mousePressed();
			break;
		case MouseEvent.MOUSE_RELEASED:
			mouseReleased();
			break;
		case MouseEvent.MOUSE_DRAGGED:
			mouseDragged();
			break;

		default:
			break;
		}
	}
	
	public void mousePressed() {
		mdx = pa.mouseX;
		mdy = pa.mouseY;
	}
	
	public void mouseDragged() {
		if (pa.mouseButton==LEFT) {
			camRotOffY = (pa.mouseX-mdx)*0.01f;
			camOffY = -pa.mouseY+mdy;
		} else if (pa.mouseButton==RIGHT) {
			distanceOff = pa.mouseY-mdy;
		}
	}

	public void mouseReleased() {
		if (pa.mouseButton==LEFT) {
			camY += camOffY;
			camOffY = 0;
			camRotY += camRotOffY;
			camRotOffY = 0;
		} else if (pa.mouseButton==RIGHT) {
			distance += distanceOff;
			distanceOff = 0;
		}
	}
}
