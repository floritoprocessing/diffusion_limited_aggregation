import processing.core.PApplet;
import processing.core.PVector;


public class Particle extends PVector {
	
	public static Particle createAtEdge(Settings settings) {
		Particle p = createEverywhere(settings);
		int edge = (int)(Math.random()*6);
		switch (edge) {
		case 0:
			p.x = settings.lowerLimit.x;
			break;
		case 1:
			p.x = settings.highLimit.x;
			break;
		case 2:
			p.y = settings.lowerLimit.y;
			break;
		case 3:
			p.y = settings.highLimit.y;
			break;
		case 4:
			p.z = settings.lowerLimit.z;
			break;
		case 5:
			p.z = settings.highLimit.z;
			break;

		default:
			break;
		}
		return p;
	}
	public static Particle createEverywhere(Settings settings) {
		// position
		float x = settings.lowerLimit.x + (float)(Math.random() * (settings.highLimit.x-settings.lowerLimit.x));
		float y = settings.lowerLimit.y + (float)(Math.random() * (settings.highLimit.y-settings.lowerLimit.y));
		float z = settings.lowerLimit.z + (float)(Math.random() * (settings.highLimit.z-settings.lowerLimit.z));
		Particle out = new Particle(settings, x, y, z);
		// speed
		out.velocity.set(randomSpeed(settings.SPEED));
		return out;
	}
	private static PVector randomSpeed(float speed) {
		float x = (float)(Math.random()*speed*(Math.random()<0.5?-1:1));
		float y = (float)(Math.random()*speed*(Math.random()<0.5?-1:1));
		float z = (float)(Math.random()*speed*(Math.random()<0.5?-1:1));
		PVector out = new PVector(x,y,z);
		float mag = out.mag();
		if (mag!=0) {
			out.mult(speed/mag);
		}
		return out;
	}
	
	private final Settings settings;
	
	private PVector velocity = new PVector();
	
	private boolean alive = true;
	
	private Particle(Settings settings, float x, float y, float z) {
		super(x, y, z);
		this.settings = settings;
	}

	public void draw(PApplet pa) {
		pa.point(x,y,z);
	}

	public void move() {
		PVector rnd = randomSpeed(settings.SPEED);
		rnd.mult(settings.RANDOM);
		velocity.mult(1-settings.RANDOM);
		velocity.add(rnd);
		float mag = velocity.mag();
		if (mag!=0) {
			velocity.mult(settings.SPEED/mag);
		}
		add(velocity);
	}

	public boolean outsideSpace() {
		if (x<settings.lowerLimit.x || x>settings.highLimit.x
				|| y<settings.lowerLimit.y || y>settings.highLimit.y
				|| z<settings.lowerLimit.z || z>settings.highLimit.z) {
			return true;
		}
		return false;
	}
	
	
}
