class Dust {
  
  float x, y;
  float rd, sp;
  boolean dead=false;
  
  Dust() {
    init();
  }
  
  void init() {
    spawnPoint(width/4,height/4);
    rd = random(TWO_PI);
    sp = DustSettings.SPEED;
  }
  
  void spawnEdge() {
    float pos = random(2*(width+height));
    if (pos<width) {
      x = pos;
      y = 0;
    } else if (pos<width+height) {
      x = width;
      y = pos-width;
    } else if (pos<width+height+width) {
      x = pos-width-height;
      y = height;
    } else {
      x = 0;
      y = pos-height-2*width;
    }
  }
  
  void spawnPoint(float x, float y) {
    this.x = x;
    this.y = y;
  }
  
  void spawnRandom() {
    x = random(width);
    y = random(height);
  }
  
  void move() {
    rd += random(-DustSettings.TURN,DustSettings.TURN);
    x += sp*cos(rd) + DustSettings.WIND_X;
    y += sp*sin(rd) + DustSettings.WIND_Y;
    edgeReInit();
  }
  
  void edgeReInit() {
    if (x<0||x>=width||y<0||y>+height) {
      init();
    }
  }
  
  void edgeWrap() {
    if (x<0) x+=width;
    if (x>=width) x-=width;
    if (y<0) y+=height;
    if (y>=height) y-=height;
  }
  
  void traceAndKill(PImage img) {
    int ix = (int)x;
    int iy = (int)y;
    for (int sy=iy-1; sy<=iy+1; sy++) {
      for (int sx=ix-1; sx<=ix+1; sx++) {
        if ((img.get(sx,sy)&0xff)==255) {
          img.set(ix,iy,0xffffffff);
          dead=true;
          return;
        }
      }
    }
  }
  
  void draw() {
    set((int)x,(int)y,0xff808080);
  }
  
}
