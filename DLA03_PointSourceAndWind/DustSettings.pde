class DustSettings {

  static final int EMITTER = 50;
  static final int MAX_DUST = 30000;
  static final float SPEED = 1;
  static final float TURN = 120 * PI/180.0;
  static final float WIND_X = 0.3;
  static final float WIND_Y = -0.1;
  
  String[] toStrings() {
    String[] out = new String[6];
    out[0] = "static final int EMITTER = "+EMITTER+";";
    out[1] = "static final int MAX_DUST = "+MAX_DUST+";";
    out[2] = "static final float SPEED = "+SPEED+";";
    float deg = TURN * 180/PI;
    out[3] = "static final float TURN = "+deg+" * PI/180.0;";
    out[4] = "static final float WIND_X = "+WIND_X+";";
    out[5] = "static final float WIND_Y = "+WIND_Y+";";
    return out;
  }
  
}
