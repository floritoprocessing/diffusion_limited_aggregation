class Settings {
  
  final int MAX_DUST = 50000;

  final EmitterSetting[] EMITTER_TYPES = {
    new EmitterSetting(50,  15*PI/180.0, 0.5, color(255,128,32)),
    new EmitterSetting(50,  45*PI/180.0, 1.5, color(32,255,128)),
    new EmitterSetting(50, 120*PI/180.0, 3, color(128,32,255))
  };
  
  final float WIND_X = 0;//0.2;
  final float WIND_Y = 0;//.11;
  
  String[] toStrings() {
    ArrayList out = new ArrayList();
    out.add("final int MAX_DUST = "+MAX_DUST+";");
    out.add("final EmitterSetting[] EMITTER_TYPES = {");
    for (int i=0;i<EMITTER_TYPES.length;i++) {
    out.add("  new EmitterSetting("+EMITTER_TYPES[i].amount
            +",  "+(EMITTER_TYPES[i].turn*180.0/PI)+"*PI/180.0"
            +", "+EMITTER_TYPES[i].speed
            +", color("+red(EMITTER_TYPES[i].colour)+","+green(EMITTER_TYPES[i].colour)+","+blue(EMITTER_TYPES[i].colour)+")),");
    }
    out.add("};");
    out.add("final float WIND_X = "+WIND_X+";");
    out.add("final float WIND_Y = "+WIND_Y+";");
    return (String[])out.toArray(new String[0]);
  }
  
}
