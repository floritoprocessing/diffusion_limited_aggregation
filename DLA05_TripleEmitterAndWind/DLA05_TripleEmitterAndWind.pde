/*
DLA - Diffusion Limited Aggregation
*/

Settings settings = new Settings();

ArrayList cloud = new ArrayList();
Emitter[] emitter = new Emitter[3];
PImage canvas;

boolean showDust = true;

String date = "DLA05_"
  + nf(year(),4)+nf(month()+1,2)+nf(day(),2)+"_"
  + nf(hour(),2)+nf(minute(),2)+nf(second(),2);
boolean settingsSaved=false;

PFont font;

void setup() {
  size(1024,1024);
  frameRate(1000);
  
  font = loadFont("Verdana-Bold-12.vlw");
  textFont(font,12);
  
  canvas = createImage(width,height,RGB);
  for (int x=-1;x<=1;x++) for (int y=-1;y<=1;y++) {
    canvas.set((int)(width/2)+x,(int)(height/2)+y,color(255));
  }
  
  
  for (int i=0;i<emitter.length;i++) {
    float rd = (float)i/emitter.length*TWO_PI;
    float rad = width*0.4;
    emitter[i] = new Emitter(width/2+rad*cos(rd),height/2+rad*sin(rd),settings.EMITTER_TYPES[i]);
  }
  
}

void keyPressed() {
  if (key=='s') {
    showDust = !showDust;
  }
  else if (key=='S') {
    saveFrameWithSettings();
  }
}

void saveFrameWithSettings() {
 
  String frameNr = nf(frameCount,6);
  String imageName = date+"_frame"+frameNr+".bmp";
  println("Saving to \""+imageName+"\"");
  canvas.save(imageName);
  
  if (!settingsSaved) {
    println("Saving settings to \""+date+"_settings.txt\"");
    saveStrings(date+"_settings.txt",settings.toStrings());
    settingsSaved = true;
  }
  
}

void draw() {
  
  /*
  *  emitter
  */
  for (int i=0;i<emitter.length;i++) {
    emitter[i].create();
  }
 
  /*
  *  Trace dust
  */
  for (int i=0;i<cloud.size();i++) {
    ((Dust)cloud.get(i)).trace(canvas);
  }
  
  /*
  *  Remove dead dust
  */
  for (int i=0;i<cloud.size();i++) {
    if (((Dust)cloud.get(i)).dead) {
      cloud.remove(i--);
    }
  }
  
  image(canvas,0,0);
  
  if (showDust) {
    for (int i=0;i<cloud.size();i++) {
      ((Dust)cloud.get(i)).draw();
    }
  }
  
  for (int i=0;i<cloud.size();i++) {
    ((Dust)cloud.get(i)).move();
  }
  
  fill(255,0,0);
  text("fps: "+nf(frameRate,2,1)+", frame "+frameCount
    +", dustAmount: "+cloud.size(),15,height-15);
  
  if (frameCount==1||frameCount%1000==0) {
    saveFrameWithSettings();
  }
}
